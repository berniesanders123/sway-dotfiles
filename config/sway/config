
set $mod Mod4

# Gaps
gaps inner 28
gaps outer 0


default_border none
default_floating_border none


# Wallpaper
output "*" bg $HOME/.config/wallpapers/981858.png fill
# autostart
exec autotiling
exec --no-startup-id xsettingsd &
exec setxkbmap -layout us
exec setxkbmap -option 'grp:alt_shift_toggle'
exec  --no-startup-id bitwarden'
exec --no-startup-id mako
exec swayidle -w \
timeout 300 'swaylock-blur' \
before-sleep 'swaylock-blur'
exec ~/.config/sway/battery_notify.sh
exec wl-paste -t text --watch clipman store
seat seat0 xcursor_theme Breeze_Snow 28
# Menu
bindsym $mod+d exec wofi --show drun -lines 12 -padding 18 -location 0 -sidebar-mode --columns 2

# Network manager applet
exec nm-applet --indicator

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 8

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# exit sway
bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

# start a terminal
bindsym $mod+Return exec konsole

# kill focused window
bindsym $mod+shift+q kill

# screenshot
bindsym $mod+x exec grim

# emacs
bindsym $mod+z exec emacsclient --create-frame

# wofer
bindsym $mod+g exec ~/.config/wofer/wofer.sh
# clipboard history
bindsym $mod+t exec clipman pick -t wofi -T'-lines 12 -padding 18 -location 0 -sidebar-mode'
# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+p split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

bindsym $mod+9 exec --no-startup-id wlogout

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+0 move container to workspace $ws10

# programs on specific workspaces
assign [class="Hyperspace Desktop"] $ws6

# volume control
bindsym XF86AudioRaiseVolume exec "amixer set Master 5%+ unmute"
bindsym XF86AudioLowerVolume exec "amixer set Master 5%- unmute"
bindsym XF86AudioMute exec "amixer set Master toggle"

# brightness control
bindsym XF86MonBrightnessDown exec brightnessctl set 15%-
bindsym XF86MonBrightnessUp exec brightnessctl set +15%

# media control
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

# Open specific applications in floating mode
for_window [title="alsamixer"] floating enable border pixel 1
for_window [class="calamares"] floating enable border normal
for_window [class="Clipgrab"] floating enable
for_window [title="File Transfer*"] floating enable
for_window [class="fpakman"] floating enable
for_window [class="Galculator"] floating enable border pixel 1
for_window [class="GParted"] floating enable border normal
for_window [title="i3_help"] floating enable sticky enable border normal
for_window [class="Lightdm-settings"] floating enable
for_window [class="Lxappearance"] floating enable sticky enable border normal
for_window [class="Manjaro-hello"] floating enable
for_window [class="Manjaro Settings Manager"] floating enable border normal
for_window [title="MuseScore: Play Panel"] floating enable
for_window [class="Nitrogen"] floating enable sticky enable border normal
for_window [class="Oblogout"] fullscreen enable
for_window [class="octopi"] floating enable
for_window [title="About Pale Moon"] floating enable
for_window [class="Pamac-manager"] floating enable
for_window [class="Pavucontrol"] floating enable
for_window [class="qt5ct"] floating enable sticky enable border normal
for_window [class="Qtconfig-qt4"] floating enable sticky enable border normal
for_window [class="Simple-scan"] floating enable border normal
for_window [class="(?i)System-config-printer.py"] floating enable border normal
for_window [class="Skype"] floating enable border normal
for_window [class="Timeset-gui"] floating enable border normal
for_window [class="(?i)virtualbox"] floating enable border normal
for_window [class="Xfburn"] floating enable

#Set theme
exec_always {
    gsettings set org.gnome.desktop.interface gtk-theme Sweet-Dark
    gsettings set org.gnome.desktop.wm.preferences theme Sweet-Rainbow
    gsettings set org.gnome.desktop.interface cursor-theme Breeze-Light
}

#Borders
border_images.focused ~/.config/sway/borders/focused.png
border_images.focused_inactive ~/.config/sway/borders/focused_inactive.png
border_images.unfocused ~/.config/sway/borders/unfocused.png
border_images.urgent ~/.config/sway/borders/urgent.png

#Autostart
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec /home/josiahninan/hud-menu/hud-menu-service.py
# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your 

bar {
    swaybar_command waybar
}
