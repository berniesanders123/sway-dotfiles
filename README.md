# Sway Dotfiles

Dotfiles for swaywm

# Screenshot


![Screenshot](screenshot.png)
# Optional dependencies
>Grim
>Mako
>Wofer
>Fish

# Required dependencies
>kvantum-qt5
>sway
>qt5-wayland
>wofi
>overpass font
>waybar
# Other
>OS: manjaro
>wm: sway
>Gtk Theme: sweet dark
>Icon Theme: papirus
>Mouse Cursor: breeze snow
>Kvantum Theme: sweet mars transparent toolbar
>Terminal: Konsole
# Firefox
Custom firefox css is at https://gitlab.com/berniesanders123/customfirefox
